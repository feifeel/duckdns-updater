# duckdns updater

Update you duckdns.org domain.

## How To use it
This script is made for /root

Git clone the repo in /root

Edit the file **duck.sh** and replace your sub domain name and add your token
**custom_name and token**

Then run this script to install the cronjob
```bash
./duck_cron.sh
```
