#!/bin/bash

# Prepare files
chmod u+x ~/duckdns/duck.sh

cat <<'EOF' >> /var/spool/cron/crontabs/root

HOME=/root
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
SHELL=/bin/bash

# update DNS
*/5 * * * * /root/duckdns/duck.sh >/dev/null 2>&1

EOF
